# Pulse for Franz
This is the Franz recipe for [Pulse](https://messenger.klinkerapps.com)

### How to create your own Franz recipes:
* [Read the documentation](https://github.com/meetfranz/plugins)
